let numBases = 30;
let baseSize = 6;
let baseSpacing = 10;
let helixRadius = 100;
let helixRotationSpeed = 0.01;
let programmingTexts = [];
let numTexts = 20;
let textSizeMin = 12;
let textSizeMax = 20;
let textSpeedMin = 1;
let textSpeedMax = 3;
let words = ["log-file4.exe", "live-server", "sad.mp3", "what.word", "Spil.exe", "Help.exe", "program-server", "Graphic.JPEG", "Memory", "CPU", "Network-output", "Input", "Output"];

function setup() {
  createCanvas(400, 400); //Creates the canvas size with the arguments
  textSize(20);
  generateProgrammingTexts();
}

function draw() {
  background(255, 215, 200)

  push();
  translate(width / 2, height);
  drawDoubleHelix(numBases, baseSize, baseSpacing, helixRadius, helixRotationSpeed);
  pop();

  for (let i = 0; i < programmingTexts.length; i++) { //The loop goes through each element of the programmingTexts array.
    programmingTexts[i].display(); //Calls the display method.
    programmingTexts[i].move(); //Calls the move method.
  }
}

function generateProgrammingTexts() {
  for (let i = 0; i < numTexts; i++) {
    let text = new ProgrammingText(random(width), random(-height, 0), words); //Creates a new instance of the class. The constructor of the ProgrammingText class takes three arguments: a random x-coordinate within the width and height and the words array
    programmingTexts.push(text); //Added the text element to programmingTexts array using push()
  }
}

class ProgrammingText {
  constructor(x, y, words) {
    this.words = words;
    this.text = this.generateText(); 
    this.x = x;
    this.y = y;
    this.size = random(textSizeMin, textSizeMax);
    this.speed = random(textSpeedMin, textSpeedMax);
  }

  generateText() {
      let randomIndex = floor(random(words.length));
  return words[randomIndex];
  }

  display() {
    textAlign(CENTER, CENTER); //Centeres the alignment of the text
    textSize(this.size); // Sets the size of the text. It is determined by the size value which is random in the constructor.
    fill(255, 50);
    text(this.text, this.x, this.y); //Text function with three arguments. The this.text property contains the actual text content to be displayed, while this.x and this.y specify the position where the text should appear on the canvas.
    fill(0, 50); // Shadow color (black with reduced opacity)
    text(this.text, this.x + 2, this.y + 2); // Offset the shadow slightly
  }

  move() {
    this.y += this.speed; // This line increments the y coordinate of the text object by the value stored in its speed property.
    if (this.y > height) {// If the text goes off the screen, reset its position to the top
      this.y = random(-100, 0); //Its y coordinate is greater than the height of the canvas. this line resets the y coordinate to a random position above the canvas
      this.x = random(width); //This line resets the x coordinate to a random position within the width of the canvas.
      this.text = this.generateText(); //This ensures that when the text reaches the top again, it displays a new set of randomly selected words
    }
  }
}

function drawDoubleHelix(numBases, baseSize, baseSpacing, helixRadius, rotationSpeed) {
  //These angles will be used to determine the positions of the ellipses that make up the double helix later in the function. By multiplying millis() by rotationSpeed, the helix rotates over time. 
  //The negative sign in angle2 indicates rotation in the opposite direction.
  let angle1 = millis() * rotationSpeed;
  let angle2 = -millis() * rotationSpeed;

  for (let i = 0; i < numBases; i++) {
    //Inside the loop, calculations are performed to determine the positions of the ellipses
    let x1 = cos(angle1 + i * TWO_PI / numBases) * helixRadius;
    let y1 = map(i, 0, numBases, 0, -height); //This maps each base's index to a vertical position from top to bottom of the canvas.

    let x2 = cos(angle2 + i * TWO_PI / numBases) * helixRadius;
    let y2 = map(i, 0, numBases, 0, -height);

    fill(200, 0, 0); //Colors
    noStroke();
    ellipse(x1, y1, baseSize, baseSize); //Draw the ellipses

    fill(0, 0, 255);
    ellipse(x2, y2, baseSize, baseSize);
  }
}
