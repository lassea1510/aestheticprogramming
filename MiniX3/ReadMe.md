# ***Mini-X 3***
### ***What Have You Produced?***
My code creates a visualization inspired by a DNA double helix, with programming-related text elements falling down the screen. I wanted to do an artistic angle on the the Throbber to make it less tedious, so a tried drawing interactive dynamics to engage/hypnotice users. The double helix structure should aymbolize the progression of the Throbber, reflecting the intricacies of programming, while the falling text elements represent the dynamic nature of coding languages and concepts.


![This is a static picture of my project](/MiniX3/MiniX3.png)

[You can run the program here](https://lassea1510.gitlab.io/aestheticprogramming/MiniX3)

[You can see the code here](https://gitlab.com/lassea1510/aestheticprogramming/-/blob/main/MiniX3/MiniX3.js)

### ***What are the time-related syntaxes/functions that you have used in your program, and why have you used them in this way? How is time being constructed in computation (refer to both the reading materials and your coding)?*** ###
I use the millis function as my time-related function. This function returns the number of milliseconds since the program started running. It's used to create a dynamic effect with my design specifically the time-based rotation for the double helix and by multipliying the rotationSpeed it creates the rotation effect on the helix design. By utilizing this function, I can create animations that change over time, such as the rotation. The setup() and draw() also use some time-related mechanics since the setup() function runs once when the program starts, while the draw() function continuously executes, redrawing the canvas to create the animation.

In computational terms, time is often represented using numerical values that increment or decrement based on a predefined unit of measurement, such as milliseconds, seconds, or frames. These values are typically obtained from system clocks or timers in different libararies and framworks. By manipulating these time-related values, you are able to some create dynamic and interactive applications that can change according to the time-related values.

### ***What are the time-related syntaxes/functions that you have used in your program, and why have you used them in this way? How is time being constructed in computation (refer to both the reading materials and your coding)?*** ###
Throbbers are very present digital interfaces like YouTube's video streaming or Facebook's feed loading, and they serve as visual cues indicating ongoing activity, managing user expectations, and providing feedback on system responsiveness. I personally have seen it alot on video streaming platforms like Youtube. While they communicate that processes are underway, they may obscure the actual duration or complexity of tasks, potentially hiding underlying issues or delays. 

There arent much feedback on the throbber itself, and it quickly becomes annoying to be presented with. In contrast, a double helix design like my throbber, reminiscent of DNA structure, could introduce a unique twist to throbbers by symbolizing the progression with the files being shown in the background. By incorporating rotating helices i wanted to create a more visually engaging representation of ongoing tasks to keep the useres attention without it being basic or annoying to experience. A dynamic approach could enhance user understanding while maintaining engagement during wait times, thus redefining throbbers as not just indicators of activity, but as interactive elements that enhance the overall user experience. Feedback could also be a element that could make Throbbers more "likeable. To characterize throbbers differently, designers could infuse them with more contextual feedback, integrating progress bars or descriptive labels to offer users clearer insights into ongoing tasks. Additionally, customization to align with brand identity or the incorporation of interactive elements could transform throbbers into more engaging and informative components.

### ***What could be improved?***
While the visualization is what i wanted, there may be opportunities to enhance its visuals. This could include refining the aesthetics of the double helix structure and the rotation it self, experimenting with different color schemes or visual effects, or adding visual feedback to indicate user interactions.


## References & Materials
[Work by Victor Rasmussen, 2022] (https://gitlab.com/VRAS/aestheticprogramming/-/blob/main/minix3/Readme.md)

[millis()] (https://p5js.org/reference/#/p5/millis)

