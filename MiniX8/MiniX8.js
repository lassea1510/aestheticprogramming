let cols = 4;
let rows = 3; 
let boxSize = 160;
let margin = 20;
let images = [];
let pokemonImages = [];
let starSigns = ["Aries", "Capricorn", "Pisces", "Taurus", "Gemini", "Leo", "Scorpio", "Virgo", "Cancer", "Libra", "Sagittarius", "Aquarius"];
let months = ['21.3-19.4', '22.12-19.1', '19.2-20.3','20.4-20.5', '21.5-20.6', "23.7-23.8", '24.10-22.11', '24.8-23.9', '22.6-22.7', '24.9-23.10', '23.11-21.12', '21.1-19.2'];
let pokemonNames = ["victini", "shaymin-land", "articuno", "jirachi", "celebi", "zapdos", "meloetta-aria", "keldeo-ordinary", "mew", "moltres", "pikachu", "manaphy"];
let clickedBoxIndex = -1;

function preload() {
  // Load your images here
  //push is pushing the images into an empty array
  for (let i = 1; i <= cols * rows; i++) {
    images.push(createImg('images/image' + i + '.png'));
    pokemonImages.push(createImg('pokemonImages/pokemon'+ i + '.png'))
  }

  pokemonBackground = loadImage("pokemonBackground.jpg");
  pokeBallImage = loadImage("pokeBallImage.png");
}

function setup() {
  createCanvas(windowWidth, windowHeight);
}

function draw() {
  background(pokemonBackground);
  noLoop();


  //Creating the headline:
  textSize(65);
  stroke(42, 117, 187);
  strokeWeight(5);
  fill(255, 203, 5);
  textStyle(BOLDITALIC);
  textAlign(CENTER);
  text("Find your spirit-Pokémon", width/2, height/2 - 300);

  //creating the grid, where the box picture and the text are located
  let gridWidth = cols * (boxSize + margin) - margin;
  let gridHeight = rows * (boxSize + margin) - margin;
  let startX = (width - gridWidth) / 2;
  let startY = (height - gridHeight) / 2 + 30;

  let index = 0;

  //nested loop to iterate over a grid boxes 
    //it loops over each column('i') and each row ('j') within those columns 
    //for each comnination of column and row, it calculates the x and y coordinates of the top-left corner of a box in the grid.
      //these coordinates are calculated based on the start position (startX, startY), the size of each box (boxSize), and the margin between the boxes ('margin')
    //it then calls the function drawBox(), with the calculated coordinates, an image, a star sign, a month and an index.
    //the index variable is incremented to move through some array or collection of data associated with each box.
  for (let i = 0; i < cols; i++) {
    for (let j = 0; j < rows; j++) {
      let x = startX + i * (boxSize + margin);
      let y = startY + j * (boxSize + margin);
      drawBox(x, y, images[index], starSigns[index], months[index], index);
      index++;
    }
  }
}

function drawBox(x, y, img, textAbove, textBelow, index) {
  fill(255);
  rect(x, y, boxSize, boxSize);

  let textX = x + boxSize / 2;
  let textYAbove = y + 20;
  let textYBelow = y + boxSize - 15;
  
  textAlign(CENTER, CENTER);
  textSize(14);
  
  text(textAbove, textX, textYAbove);
  text(textBelow, textX, textYBelow);
  image(img, x + 25, y + 25, boxSize - 50, boxSize - 50);
}

function mouseClicked() {
  // Calculate the starting position of the grid
  let gridStartX = (windowWidth - cols * (boxSize + margin) + margin) / 2 + 30;
  let gridStartY = (windowHeight - rows * (boxSize + margin) + margin) / 2 + 30;

  // Check if the mouse click is within the boundaries of the grid
  if (mouseX >= gridStartX && mouseX < gridStartX + cols * (boxSize + margin) &&
  mouseY >= gridStartY && mouseY < gridStartY + rows * (boxSize + margin)) {
    
  // Calculate the column and row index of the clicked box
  let colIndex = floor((mouseX - gridStartX) / (boxSize + margin));
  let rowIndex = floor((mouseY - gridStartY) / (boxSize + margin));

  // Calculate the index of the clicked box
  clickedBoxIndex = colIndex + rowIndex * cols;

  // Print the index of the clicked box
  print("Clicked on box with index:", clickedBoxIndex);


  //Calling the information from the API
  let pokemonName = pokemonNames[clickedBoxIndex];
  fetchPokemonData(pokemonName)
    .then(pokemonData => {
      // Store the fetched Pokémon data
      displayedPokemonData = pokemonData;
      drawPokemonBox(pokemonData);
    })
    .catch(error => {
      console.error("Error fetching Pokémon data:", error);
    });
  }

  if(mouseClicked){
    background(pokemonBackground, 150);
  }


}

async function fetchPokemonData(pokemonName) {
  const response = await fetch(`https://pokeapi.co/api/v2/pokemon/${pokemonName}`);
  const data = await response.json();
  return {
    name: data.name,
    types: data.types.map(type => type.type.name),
    stats: data.stats.map(stat => ({
      name: stat.stat.name,
      value: stat.base_stat
    }))
  };
}

function drawPokemonBox(pokemon) {
  let x = 700;
  let y = 310;
  let boxWidth = 300;
  let boxHeight = 300;
  fill(255, 203, 5);
  stroke(42, 117, 187);
  rect(x, y, boxWidth, boxHeight);
  textSize(20);
  text(`Name: ${pokemon.name}`, x + 150, y + 30);
  textSize(14);
  text("Types:", x + 80, y + 80);
  for (let i = 0; i < pokemon.types.length; i++) {
    text(pokemon.types[i], x + 80, y + 100 + i * 20);
  }
  text("Stats:", x + 210, y + 80);
  for (let i = 0; i < pokemon.stats.length; i++) {
    text(`${pokemon.stats[i].name}: ${pokemon.stats[i].value}`, x + 210, y + 100 + i * 20);
  }
    //calling the pokemonImages from the preload function
    image(pokemonImages[clickedBoxIndex], x - 350, y - 45, 350, 400);

    //pokeball is getting called from the preload function
    image(pokeBallImage, 725, 470, 100, 100);
}