 ***MiniX5***
### ***Project - The Sinus Wave***
My program utilizes sine and cosine functions to create sinusoidal patterns in both color variation and depth modulation. Sine and cosine functions are fundamental to generating sinusoidal waves, which are prevalent in various aspects of mathematics, physics, and art. I had seen these waves at the beginning of our work with p5.js, and I wanted to try to recreate them, since they have this beautiful aethetic feel.  In this case, they contribute to the smooth oscillations of colors and the pulsating depth effect, adding some visually appealing elements to the generative artwork.

![This is a static picture of my project](/MiniX5/Sinus_Wave.png)

[You can run the program here](https://lassea1510.gitlab.io/aestheticprogramming/MiniX5)

[You can see the code here](https://gitlab.com/lassea1510/aestheticprogramming/-/blob/main/MiniX5/MiniX5.js)

### ***What are the rules in your generative program? Describe how your program performs over time? How do the rules produce emergent behavior?***
This generative program creates a 3D sinus wave pattern that changes over time through the use of several rules. First Shapes rotate around the x-axis, creating a sense of movement and perspective. Secondly, colors alternate based on the position of shapes in the loop and the frame count, leading to a shifting color palette in the design. The number of sides of each shape increases with the loop's iteration, while pulsating depth variation adds dimensionality.

These rules work together to produce the emergent behavior. As the animation of the sinus wave progresses, the combination of rotating shapes, changing colors, and pulsating depth creates the full visual display. Each shape contributes to the overall pattern, resulting in a dynamic generative composition.

Once the program is running, it operates autonomously, continuously generating new patterns and colors without direct input from the programmer. The program's behavior emerges from the interaction of simple rules, such as rotation, color variation, shape variation, and depth modulation. Despite the lack of direct intervention, the program exhibits autonomy in its ability to create complex and evolving visual patterns.

## References & Materials
[Sine wave structures in p5.js | Coding Project #1] (https://www.youtube.com/watch?v=vmhRlDyPHMQ)
