//The variables needed for the sliders and buttons. They get intialized later in the code.
let eyeSizeSlider, mouthSizeSlider, eyebrowRaiseRightSlider, eyePositionSlider, eyebrowRaiseLeftSlider, mouthBendSlider;
let happyButton, sadButton, neutralButton;

//Bolean Variables defined, this is the initial values when you open the program.
let isHappy = false;
let isSad = false;
let isNeutral = true;


function setup() {
  createCanvas(600, 400);
  background(200)

  //Mostly Inline Styling for creating a container for having the sliders
  let sizeSliderContainer = createDiv('');
  sizeSliderContainer.position(width / 4 - 50, height / 4 - 80);
  sizeSliderContainer.style('background-color', '#f0f0f0');
  sizeSliderContainer.style('padding', '10px');
  sizeSliderContainer.style('border-radius', '10px');

  //This is defining the range of the sliders with the parameters
  eyeSizeSlider = createSlider(5, 60, 30); //The first value is the mininium, the second explain the max, while the last is optional but it gives an initial value of the slider.
  createSliderWithLabel("Eye Size", sizeSliderContainer, eyeSizeSlider); 
  mouthSizeSlider = createSlider(10, 100, 50);
  createSliderWithLabel("Mouth Size", sizeSliderContainer, mouthSizeSlider); //this function call creates a slider with a label titled "Mouth Size", and it's placed inside the sizeSliderContainer container. The slider element itself is stored in the mouthSizeSlider variable.
  eyebrowRaiseRightSlider = createSlider(-20, 20, 0);
  createSliderWithLabel("Eyebrow Raise (Right)", sizeSliderContainer, eyebrowRaiseRightSlider);

  let bendSliderContainer = createDiv('');
  bendSliderContainer.position(width / 4 - 50, height / 4 + 220);
  bendSliderContainer.style('background-color', '#f0f0f0');
  bendSliderContainer.style('padding', '10px');
  bendSliderContainer.style('border-radius', '10px');
  
  eyePositionSlider = createSlider(-20, 20, 0);
  createSliderWithLabel("Eye Position", bendSliderContainer, eyePositionSlider);
  mouthBendSlider = createSlider(-20, 20, 0);
  createSliderWithLabel("Mouth Bend", bendSliderContainer, mouthBendSlider);
  eyebrowRaiseLeftSlider = createSlider(-20, 20, 0);
  createSliderWithLabel("Eyebrow Raise (Left)", bendSliderContainer, eyebrowRaiseLeftSlider);

  let buttonsContainer = createDiv('');
  buttonsContainer.position(100, height / 2.5);
  buttonsContainer.style('background-color', '#f0f0f0');
  buttonsContainer.style('padding', '10px');
  buttonsContainer.style('border-radius', '10px');
  
  happyButton = createButton('Happy');
  happyButton.parent(buttonsContainer); //This indicates that the "happyButton" is the child of "buttonsContainer". Meaning that happyButton will be stored inside buttonsContainer
  happyButton.style('display', 'block');
  happyButton.mousePressed(setHappyExpression);

  //This is for creating space between the buttons on the left container.
  let spaceDiv1 = createDiv('');
  spaceDiv1.parent(buttonsContainer); 
  spaceDiv1.style('height', '10px'); 

  sadButton = createButton('Sad');
  sadButton.parent(buttonsContainer);
  sadButton.style('display', 'block');
  sadButton.mousePressed(setSadExpression);

  let spaceDiv2 = createDiv('');
  spaceDiv2.parent(buttonsContainer);
  spaceDiv2.style('height', '10px');

  neutralButton = createButton('Neutral');
  neutralButton.parent(buttonsContainer);
  neutralButton.style('display', 'block');
  neutralButton.mousePressed(setNeutralExpression);
}

function createSliderWithLabel(label, container, slider) {
  let sliderContainer = createDiv('');
  sliderContainer.parent(container);
  sliderContainer.style('display', 'inline-block');

  let titleLabel = createDiv(label);
  titleLabel.parent(sliderContainer);
  
  slider.parent(sliderContainer);
}

function setHappyExpression() { //Changes the boolean values according to what was button pressed prior.
  isHappy = true;
  isSad = false;
  isNeutral = false;
}

function setSadExpression() {
  isHappy = false;
  isSad = true;
  isNeutral = false;
}

function setNeutralExpression() {
  isHappy = false;
  isSad = false;
  isNeutral = true;
}

function draw() {
  background(220);
  
  fill(255, 255, 0);
  ellipse(width/2, height/2, 200, 200);
  
  drawEyes(width/2 - 40, height/2 - 20);
  drawEyes(width/2 + 40, height/2 - 20);
  
  drawMouth(width/2, height/2 + 20);
  
  drawEyebrows(width/2 - 40, height/2 - 40);
  drawEyebrows(width/2 + 40, height/2 - 40);
}

function drawEyes(x, y) {
  fill(0);
  let eyeSize = eyeSizeSlider.value(); //retrieves the current value of the slider element with ".value()"
  let eyePosition = eyePositionSlider.value();
  ellipse(x, y + eyePosition, eyeSize, eyeSize);
}

function drawEyebrows(x, y) {
  noFill();
  stroke(0);
  strokeWeight(2);
  let eyebrowRaiseRight = eyebrowRaiseRightSlider.value();
  let eyebrowRaiseLeft = eyebrowRaiseLeftSlider.value();
  line(x - 15, y - 10 - eyebrowRaiseLeft, x + 15, y - 10 + eyebrowRaiseRight);
}

function drawMouth(x, y) {
  let mouthSize = mouthSizeSlider.value();
  let mouthBend = mouthBendSlider.value();
  noFill();
  stroke(0);
  strokeWeight(4);
  if (isHappy) {
    arc(x, y, mouthSize, mouthSize, 0 + radians(mouthBend), PI - radians(mouthBend), OPEN);
  } else if (isSad) {
    arc(x, y + mouthSize / 2, mouthSize, mouthSize / 2, PI + radians(mouthBend), TWO_PI - radians(mouthBend), OPEN);
  } else {
    line(x - mouthSize / 2, y, x + mouthSize / 2, y);
  }
}
// "0 + radians(mouthBend)" Converts the value of mouthBend from degrees to radians and it is the starting angle of the arc.
// "OPEN" specifies that the arc should be drawn with open ends rather than being closed, meaning it will not connect the start and stop points with straight lines.