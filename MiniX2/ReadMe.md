# ***Mini-X 2***
### ***What Have You Produced?***
My program itself is an interactive prototype application where users can customize various aspects of an emoji's facial features, including eye size, eyebrow bend, mouth expression. I have made sliders are to enable the user to adjust these features, while buttons allow users to switch between different facial expressions (happy, sad, neutral) to be able to express themselves though emojis.

I played around with some interactivity features in p5.js to experiment on how it works in p5.js. The interactivity comes in the form of createSliders and createButtons functions. I ofcourse also experimented with drawing shpes through p5.js primarily using (line, eclipse and arc). My project felt much more similar that what we have used during the Programming Fundamentals. It felt similar in structure on how it has HTML, CSS and Javascript features that felt familiar.

![This is a static picture of my project](/MiniX2/MiniX2_-_Default.png)

[You can run the program here](https://lassea1510.gitlab.io/aestheticprogramming/MiniX2)

[You can see the code here](https://gitlab.com/lassea1510/aestheticprogramming/-/blob/main/MiniX2/MiniX2.js)

### ***What is the social and cultural context of my program***
My emoji, through its various facial features and expressions controlled by sliders and buttons, serves as a tool for users to express themselves within a digital medium. In terms of representation, it allows individuals to create an avatar or symbol that may reflect aspects of their identity, emotions, or personality.

The reason behind my project was to create an emoji where you can change the facial features to your liking. It was both to show more representation in identity through these features as well as be able to show degrees in emotions, since many emoji dont show the "real" emotion degrees. Now you can adapt the smile with how "happy" or "sad" a person is currently feeling to better communicate through the digital medium.

The flexibility to change elements such as eye size, eyebrow tilt, and mouth expression offers a spectrum of customization possibilities. This allows for a personalized touch that can resonate with individual tastes, cultural influences, or current emotional states. Users have the freedom to fine-tune the emoji to reflect their unique physical attributes and emotional nuances.

### ***What could be improved?***
My adjustable emoji works just fine according to what I was thinking. The adjustments are quite limiting since you cant change the shape of the eyes much for example. If I were to continue working on it, I would look at adding mor adjustable features without making the interface cluttered. 

I could also add more facial features to hammer the point home futher like a nose or ears thats also important for expression. These should also have similar adjustable elements, but this emoji works more as a prototype for a proof of concept.

## References & Materials
[createSliders](https://p5js.org/reference/#/p5/createSlider)

[createButtons](https://p5js.org/reference/#/p5/createButton)

[arc()](https://p5js.org/reference/#/p5/arc)

Smiley references with some code references:
[Smiley face by cs4all](https://editor.p5js.org/cs4all/sketches/S189x9fp-),[ Smiley by mwpak](https://editor.p5js.org/mwpak/sketches/K-VRu95Tp), [Personalized Gender Emoji by Gabriel Høst Andersen  ](https://gitlab.com/9plus10savage/aesthetic-programming/-/blob/main/miniX2/ReadMe.md), [Processing 1 - Making A Smiley by Tom Smith](https://www.youtube.com/watch?v=aM7nbxJz5QI)

Primary inspiration for project:

[Are you really smiling?... by Moyu Liu](https://www.frontiersin.org/journals/psychology/articles/10.3389/fpsyg.2023.1035742/full)

[Emojis that work!... by Isabelle Boutet ](https://www.sciencedirect.com/science/article/pii/S2451958822000859)
