 ***MMiniX4***
### ***Project - Face Behind The Screen***
My project is to show a simulation of a pc screen, where there is a popup asking for permission for cookies. When the user accepts cookies the screen changes into several popups that looks like a face on the screen. Each data represents a part of the facial features. The face itself also cut out of the screen border to show how people outside the virtual space have access to the data itself, and it isnt limited to the inside of your computer.

![This is a static picture of my project](/MiniX4/ScreenShot_of_MiniX4.png)

[You can run the program here](https://lassea1510.gitlab.io/aestheticprogramming/MiniX4)

[You can see the code here](https://gitlab.com/lassea1510/aestheticprogramming/-/blob/main/MiniX4/MiniX4.js)

My code and the underlying concepts address the theme of "capture all" through the use of multiple ways of captured data with the webcam, search history or location and geographic data. I wanted to critice how quickly we accept cookies and allow people to capture data without knowing what happens to it. I wanted to consider these termporal and spatial dimensions, reflect on how much information is given just by accepting cookies. I wanted the structure to not outwardly say that the data is being stolen or used, but I want the user to react to the data being shown. Like a shock facture.

Data collection is a big deal because it touches on important stuff like privacy, ethics, who's got power, and who we are as individuals. No matter where you are, people worry about their privacy and being watched, which makes us question whether we really have a say in what happens to our information. Figuring out what's okay and what's not when it comes to gathering data depends a lot on cultural norms and values. When certain groups or organizations have more control over data, it can make existing inequalities even worse and make us all feel less trusting. Being aware of cultural differences is super important to make sure we don't accidentally hurt or misuse personal info.

## References & Materials
[The Revealing of Digital Surveillance by Josefine Pasma, 2022] (https://gitlab.com/JosefinePasma/aestheticprogramming/-/blob/main/MiniX4/Readme.md)

[Data Gathering by Josefine Stenshøj, 2022] (https://gitlab.com/josefinestenshoj/aestheticprogramming/-/blob/main/MiniX4/ReadMe.md)

[Video Capture] (https://p5js.org/examples/dom-video-capture.html)