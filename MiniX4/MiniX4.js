let acceptButton;
let dataPopups = [];
let capture;
let showPopup = true;
let backgroundImage; // Variable to store the background image

function preload() {
  // Load the background image
  backgroundImage = loadImage('WindowsBack.png');
}

function setup() {
  createCanvas(550, 500);

    // Create accept button
    acceptButton = createButton('Accept');
    acceptButton.position(width/2 - acceptButton.width/2, height/2 - acceptButton.height/2);
    acceptButton.mousePressed(triggerDataPopups);
  
    button = createButton("x")
    button.style("background", "#f22727")
    button.style("color", "#ffffff")
    button.size(20,20)
    button.position(365, 180)

  capture = createCapture(VIDEO, function() {
  capture.size(320, 240);
  capture.hide();
});
}

function draw() {
// Draw the background image
image(backgroundImage, 0, 0, width, height);

// Background color for header
fill(50);
rect(0, 0, width, 80);

// Background color for footer
fill(50);
rect(0, height - 80, width, 80);

if (showPopup) {
  // Draw the pop-up box only if showPopup is true
  noStroke();
  fill(170);
  rect(180, 180, 204, 100);

  // Draw the header line
  stroke(0);
  line(180, 200, 384, 200); // Adjusted coordinates to fit within the rectangle

  // Display text inside the rectangle
  fill(0); // Set text color to black
  textAlign(CENTER, CENTER); // Center align text horizontally and vertically
  text("Do you want to accept cookies", 180 + 204 / 2, 180 + 100 / 2); // Position text at the center of the rectangle

  // Draw buttons only if showPopup is true
  acceptButton.position(width / 2 - acceptButton.width / 2, height / 2 - acceptButton.height / 2);
  button.position(365, 180);
} else {
  // Hide buttons when showPopup is false
  acceptButton.hide();
  button.hide();
}
      
  // Display all data popups
  for (let i = 0; i < dataPopups.length; i++) {
    dataPopups[i].display();
  }
}

class Popup {
  constructor(x, y) {
    this.x = x;
    this.y = y;
    this.width = 200;
    this.height = 200;
  }

  display() {
    // Display popup window
    fill(255);
    rect(this.x, this.y, this.width, this.height);
  }
}

class LocationPopup extends Popup {
  constructor(x, y, imageUrl) {
    super(x, y, "Location");
    this.width = 200;
    this.height = 150;
    this.imageUrl = imageUrl; // Store the URL of the location image
    this.imageWidth = 200; // Set the desired width of the image
    this.imageHeight = 150; // Set the desired height of the image
  }

  display() {
    super.display();

    // Display location image with resized dimensions
    let locationImg = createImg(this.imageUrl, "");
    locationImg.size(this.imageWidth, this.imageHeight); // Resize the image
    locationImg.position(this.x, this.y); // Adjust position to fit image inside pop-up
  }
}


class WebcamPopup extends Popup {
  constructor(x, y) {
    super(x, y, "Webcam");
    this.width = 200;
    this.height = 150;
  }
  
  display() {
    super.display();
    
    // Draw the webcam stream
    image(capture, this.x, this.y, 200, 150); // Adjust position and size as needed
  }
}

class SearchHistoryPopup extends Popup {
  constructor(x, y, width) {
    super(x, y, "Search History");
    this.width = 450;
    this.height = 150;
    this.scrollSpeed = 1;
    this.topMargin = 10; // Margin from the top of the box
    this.lineHeight = 20; // Line height for each text line
    this.textPosition = 0; // Vertical position of the text
    this.content = [
      'Checked in: Aarhus region',
      '06:50 PM: Went to Kings',
      'Texted mom: going through a divorce?',
      '20.12.2019: golden retriever recognized in photo',
      'Googled: signs of depression',
      '02:24 PM sent: aunt\'s birthday tomorrow!',
      '08:48 AM: deleted picture',
      'Spotify 9-10 PM: listened to Frank Sinatra',
      '04:04 AM: swipes through tinder',
      '2 hours ago: parked at work',
      '07:18 PM: used VISA credit card XXXX XXXX XXXX 3476',
      '01:13 PM: Searched for møbler',
      '13.02.2021 sent: looking for a new apartment?',
      'Googled: possible fracture?',
      'Watched: what I eat in a day',
      'Spent 5 hours and 38 minutes on iPhone',
      'Googled: how do you know if you\'re an alcoholic?',
      'Googled: is it normal to bleed for 10 days?',
      'Googled: is it dangerous if your dog eats an entire box of chocolate?',
      'Deep sleep from 10:47 PM to 03:35 AM',
      '12.03.2022: reached 12,307 steps'
    ];
    this.concatenatedContent = this.content.concat(this.content); // Duplicate the content for infinite scrolling
  }

  display() {
  super.display(); // Call the parent class's display method to draw the popup window

    // Move text vertically
    this.textPosition += this.scrollSpeed;

    // If text has moved past the end of the content, reset its position
    if (this.textPosition >= this.concatenatedContent.length * this.lineHeight) {
      this.textPosition = 0; // Reset to the top
    }

    // Set text alignment to left
    textAlign(LEFT);

    // Draw each line of text within the popup's boundaries
    fill(0);
    for (let i = 0; i < this.content.length; i++) {
      let y = this.y + this.height - this.topMargin - (i + 1) * this.lineHeight + this.textPosition % (this.content.length * this.lineHeight); // Calculate y position for each line
      if (y > this.y && y < this.y + this.height - this.lineHeight) {
        text(this.concatenatedContent[i], this.x + 10, y + 10); // Align text to the left and move the text area a bit to the right
      }
    }
  }
}

function triggerDataPopups() {

    // Hide the pop-up box when accept button is pressed
    showPopup = false;

  // Simulate data gathering by creating popups
  let dataTypes = ["Webcam", "Location", "Search History"];
  for (let i = 0; i < dataTypes.length; i++) {
    let dataPopup;
    if (dataTypes[i] === "Webcam") {
      dataPopup = new WebcamPopup(50, 50);
    } else if (dataTypes[i] === "Location") {
      dataPopup = new LocationPopup(300, 50, "Maps.png");
    } else {
      dataPopup = new SearchHistoryPopup(50, 300, 400);
    }
    dataPopups.push(dataPopup);
  }
}
