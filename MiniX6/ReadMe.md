 ***MiniX6***
### ***Project - The Ballon Pop (Reaction Game) ***
My game is a simple balloon-popping game implemented using p5.js. In this game, balloons of random sizes and colors float up from the bottom of the screen, and the player's objective is to pop them by clicking on them with the mouse. It then gradually gets faster as the player's click more ballons.

![This is a static picture of my project](/MiniX6/Game.png)

[You can run the program here](https://lassea1510.gitlab.io/aestheticprogramming/MiniX6)

[You can see the code here](https://gitlab.com/lassea1510/aestheticprogramming/-/blob/main/MiniX6/MiniX6.js)

### ***Describe how you program the objects and their related attributes, and the methods in your game***
The objects in my code are used to represent entities within my game's "world", such as the balloons that rise, and each object has properties that define its characteristics (like position, size, color) and methods that describe its behaviors (like moving, updating, and interacting with the objects inside the game).

The has a higher level of abstraction in a wider cultural context. If you consider the seemingly effortless act of popping a balloon in the game, since it is basically the only thing happening. Behind the scenes, though, there's lots of calculations and algorithms orchestrating the movement of balloons and detecting collisions. However, players are mostly unaware of this complexity in the first place as they focus on the sheer fun of clicking and popping balloons. This abstraction allows players to focus on the intuitive aspect of popping balloons without being burdened by the technical complexities behind the logic and code.

