let balloons = [];
let score = 0;
let balloonSpeed = 1;
let lives = 10;
let baseBalloonSpeed = 1;
let speedIncrement = 0.05;

function setup() {
  createCanvas(400, 400);
  addBalloons(5);
}

function draw() {
  background('skyblue');

  for (let balloon of balloons) {
    balloon.update();
  }

  if (random(1) < 0.03) { 
    addBalloons(1);
  }

  balloonSpeed = baseBalloonSpeed + speedIncrement * score;

  displayScore();
  displayLifeBar();

  if (lives <= 0) {
    gameOver();
  }
}

function mousePressed() {
  for (let balloon of balloons) {
    if (!balloon.bursted) {
      if (balloon.burst(mouseX, mouseY)) {
        score++;
      }
    }
  }
}

function addBalloons(num) {
  for (let i = 0; i < num; i++) {
    let x = random(width);
    let y = random(height, height + 50);
    let size = random(30, 70);
    balloons.push(new Balloon(x, y, 'red', size));
  }
}

function displayScore() {
  fill(255);
  textSize(20);
  text('Score: ' + score, 10, 30);
}

function displayLifeBar() {
  let lifeBarWidth = lives * 20;
  fill(255, 0, 0);
  rect(width - lifeBarWidth - 10, 10, lifeBarWidth, 20);
}

function gameOver() {
  textSize(32);
  fill(255, 204, 0);
  textAlign(CENTER, CENTER);
  text('Game Over!!!\nPress "space" to restart', width / 2, height / 2 - 20);
  noLoop();
}

function keyPressed() {
  if (keyCode === 32) {
    resetGame();
  }
}

function resetGame() {
  score = 0;
  lives = 10;
  balloons = [];
  addBalloons(5);
  loop();
}

class Balloon {
  constructor(x, y, colour, size) {
    this.x = x;
    this.y = y;
    this.colour = colour;
    this.size = size;
    this.bursted = false;
  }
  
  update() {
    if (!this.bursted) {
      this.display();
      this.move();
      if (this.y + this.size / 2 <= 0) {
        lives--;
        this.bursted = true;
      }
    }
  }

  display() {
    fill(this.colour);
    ellipse(this.x, this.y, this.size);
  }

  move() {
    this.y -= balloonSpeed;
  }
  
  burst(mouseX, mouseY) {
    let distance = dist(this.x, this.y, mouseX, mouseY);
    if (distance <= this.size / 2) {
      this.bursted = true;
      return true;
    }
    return false;
  }
}
