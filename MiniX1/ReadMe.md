# ***Mini-X 1***
### ***What Have You Produced?***
In the first MiniX, there wasnt much directiton, so I wanted to play around with some of this generative functions with Vector(). I wanted to have movement on my canvas to keep it more aesthetic pleasing. It doesn't include any interaction, since it's more of a visual art piece.My project is a generative random particle movement across the canvas. It uses the random function to randomize the qualities of the particle like size, position, acceleration and velocity.

![This is a static picture of my project](/MiniX1/Particles.png)

You can see the dynamic project in the link below:

[You can run the program here](https://lassea1510.gitlab.io/aestheticprogramming/MiniX1)

[You can see the code here](https://gitlab.com/lassea1510/aestheticprogramming/-/blob/main/MiniX1/MiniX1.js)


### ***The Coding Experience***
During my coding, i found using the P5.js reference list more helpful than I initially thought. The way the syntax is presented is very easy to understand. During this project I used the reference guide both for function found in p5.js as well as common programming techniques.

Whenever i felt stuck with the reference sheet. I try finding similar projects and understand their code to develop mine further. Some of the projects that I used are in the "References". I find that reading and altering code is one the best ways for me to learn and understand how to be a more effective programmer yourself. It also shows me the possibility and further my creativity.

### ***How is the coding process different from, or similar to, reading and writing text?***
Programming feels more like giving instruction and commands while "normal" writing is more fluffy in how you try to convey and express yourself. When programming things need to be more specific and concrete, so people and yourself can understand what the code is trying to do.

### ***What does code and programming mean to you, and how does the assigned reading help you to further reflect on these terms?***
I have worked with programming for some years now, both in school and during my passtime. I am nowhere near being a professional, and I still think I havent scratched the surface of what is possible with programming. I am still learning (:

I find the Aesthetics Programming book we have been reading very helpful for understanding P5.js, and the Aesthetics behind coding. The way it shows examples and explain some of the syntax makes it much easier to read than some of my prior experience reading about programming.

Understanding the possibilities of digital tools is essential when designing digital solutions to problems, and learning to code enhances a designer's comprehension of these capabilities.

## References & Materials
[How to create colored particles effect using p5js](https://dev.to/bitnagar/how-to-create-colored-particles-effect-using-p5js-easy-1nml)

[pushing particles](https://editor.p5js.org/kyeah/sketches/dHJlRUVWS)

[p5.Vector](https://p5js.org/reference/#/p5.Vector)

[random()](https://p5js.org/reference/#/p5/random)

[random2D()](https://p5js.org/reference/#/p5.Vector/random2D)
