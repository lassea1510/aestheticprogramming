let particles = [];
//"let" declares a variable named "particle". Initializes the variable particles as an empty array.

function setup() {
  //Declares a function called setup with no parameters. {} shows the body af the function.
  createCanvas(600, 400);
  //Function used to create a canvas element. It has 2 arguments specifying the width and height in pixels.
  for (let i = 0; i < 100; i++) {
    // "for" is a keyword that starts a loop that repeat until the condition returns false
    // "let i = 0" intializes a variable with the value of "0" and it is called "i". It is a loop counter.
    // The loop continues until "i" is less than 100 (i < 100;)
    particles.push(new Particle(random(width), random(height)));
    // the array called "particles" uses "push" method (Array Method - adds to the empty array)
    // Creates a new instance of the Particle class with a random x and y position within the canvas using the random() function.
    // random(width) = Generates a random number between 0 and width.
    // random(height) = Generates a random number between 0 and height.
  }
}

function draw() {
  background(0);
  //The background() function is used to set the background color of the canvas. In this case, it sets the background to black (0 represents black in terms of grayscale).
  for (let i = 0; i < particles.length; i++) {
    //This is a for loop that iterates over each particle in the particles array.
    //let i = 0: Initializes a loop counter variable
    //i < particles.length: The loop continues as long as the value of i is less than the length of the particles array.
    //i++: Increments the value of i by 1
    particles[i].update();
    //Calls the update() method of the i-th particle in the particles array.This method is responsible for updating the position and other properties of the particle.
    //Look futher down in "update"
    particles[i].display();
    //Calls the display() method of the i-th particle in the particles array.This method is responsible for displaying the particle on the canvas.
    //look futher down in "display"
  }
}

class Particle {
  constructor(x, y) {
    //The constructor method is a special method in a class that gets called automatically when an instance of the class is created.
    // x and y are parameters passed to the constructor function. It is the initial position of the particle.
    this.position = createVector(x, y);
    // this.position creates a property named position within each instance of the Particle class.
    this.velocity = p5.Vector.random2D();
    // Generates a 2D vector with a random direction. This vector represents the initial velocity of the particle.
    this.acceleration = createVector();
    // createVector() creates a 2D vector with both x and y components initialized to zero. This vector represents the initial acceleration of the particle, which is zero at the beginning.
    this.size = random(5, 15);
    // random(5, 15) generates a random number between 5 and 15, representing the size of the particle.
    this.color = color(random(255), random(255), random(255));
    //color(random(255), random(255), random(255)) generates a random color using RGB values. Each component (red, green, and blue) is set to a random value between 0 and 255.
  }

  display() {
    noStroke();
    // This function sets the stroke color to transparent. The shapes then won't have an outline.
    fill(this.color);
    // this.color represents the color property of the current particle. It's a color object generated using the color() function with random RGB values.
    ellipse(this.position.x, this.position.y, this.size);
    // Ellipse(this.position.x, this.position.y, this.size) draws an ellipse at the current position of the particle with its the size and color.
  }

  edges() {
    if (this.position.x > width) {
      this.position.x = 0;
    } else if (this.position.x < 0) {
      this.position.x = width;
      // // Checks if the x-coordinate is less than 0, indicating that the object has moved beyond the left edge of the canvas.
    }
    if (this.position.y > height) {
      this.position.y = 0;
    } else if (this.position.y < 0) {
      this.position.y = height;
    }
    // Each conditional statement checks the position of an object along the x-axis and y-axis relative to the width and height of the canvas.
    // if (this.position.x > width) = Checks if the x-coordinate of the object (this.position.x) is greater than the width of the canvas. If true, the object has moved beyond the right edge of the canvas.
  }

  update() {
    this.velocity.add(this.acceleration);
    // This line adds the current acceleration vector to the velocity vector of the particle.
    // It accelerates the particle by changing its velocity based on the current acceleration.
    this.position.add(this.velocity);
    // This line adds the current velocity vector to the position vector of the particle.
    // It updates the position of the particle based on its current velocity.
    this.acceleration.mult(0);
    // This line multiplies the acceleration vector by 0.
    // It resets the acceleration to zero after each update.
    this.edges();
    // This line calls the edges() method of the particle.
    // It ensures that the particle stays within the boundaries of the canvas.
  }
}